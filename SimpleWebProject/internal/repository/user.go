package repository

import "SimpleWebProject/internal/model"

type UserRepository struct {
}

func NewUserRepository() *UserRepository {
	return &UserRepository{}
}

func (repo *UserRepository) GetAll() (users []*model.User) {

	users = []*model.User{
		{
			ID:        1,
			FirstName: "User1",
			LastName:  "Sample1",
		}, {
			ID:        2,
			FirstName: "User2",
			LastName:  "Sample2",
		},
	}

	return
}
